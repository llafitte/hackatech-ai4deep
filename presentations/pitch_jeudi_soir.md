# I-Problème & Marché : prouvez que vous êtes experts du marché.

Encore aujourd’hui, on compte ainsi **150 000 décès** de cancer par an en France. 

## - Problème + cible

Certains cancers sont **impossibles à traiter** avec les méthodes habituelles (chirurgie, radio fréquence,  chimio) c’est notamment le cas des cancers primitif du foie qui sont difficiles d’accès et de la plupart des cancers du pancréas. 

Pour les traiter, la **solution est l’électroporation**. Elle permet de replacer les patients dans des stratégies curatives.

Cette technologie **non thermique** consiste **détruire les cellules par application d'impulsions électriques** délivrées à l’aide d’aiguilles (3 à 8 aiguilles en pratique soit 3 à 28 combinaisons)

Cette technologie est **complexe d'utilisation** et le médecin n’a **aucune visibilité sur la zone traitée**. Le nombre de rechute est plus important, fournir une visualisation peropératoire permettra d' ajuster le traitement **on line** pour minimiser les rechutes.

## - Taille de marché

A terme:
- 10 000 cancers de primitif du foie 
- 60 000 cancers de la prostate
- 58 400 cancers du sein

En 2020, 361 sites autorisé pour la radiologie interventionnelle.

=> marché en très forte augmentation 

# II-Solution : comment vous résolvez le problème

# - Proposition de valeur

AI4deep propose un **outil d’aide à la décision pour les radiologues interventionnels** afin de **traiter plus précisément** et efficacement certains types de cancers graves (foie, pancréas) difficiles d'accès, **sans risque d’endommager les tissus sains**.

## - Fonctionnement de la solution

AI4deep offre une **visualisation 3D de la zone traité** (résultat de la simulation numérique) et de la tumeur ce qui permet de **voir rapidement la pertiance du positionnement des aiguilles**.

## - Modèle économique

Le modèle économique repose sur la **vente d’un abonnement annuel** aux hôpitaux et cliniques ainsi que la **proposition de formations** pour apprendre à maîtriser l’outil.

# III-Différenciateur technologique : comment ça marche et pourquoi c'est mieux

## - Tech utilisée

C’est une **simulation numérique de distribution du champ électrique** en 3D basée sur les données anatomiques du **patient aiguilles en place**. 

**Elles complètent la technologie de l'électroporation en la rendant plus précise et efficace.**  

## - Différenciation

C’est une **innovation** qui permet de **d'optimiser une technologie déjà existante très prometteuse et de démocratiser son utilisation**

Il n'y a pas de concurrence directe.

