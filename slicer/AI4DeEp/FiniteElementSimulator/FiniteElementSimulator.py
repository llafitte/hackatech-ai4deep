import logging
import os

import vtk
import qt
import sitkUtils
import SimpleITK as sitk

import slicer
from slicer.ScriptedLoadableModule import *
from slicer.util import VTKObservationMixin


#
# FiniteElementSimulator
#

class FiniteElementSimulator(ScriptedLoadableModule):
    """Uses ScriptedLoadableModule base class, available at:
    https://github.com/Slicer/Slicer/blob/main/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent):
        ScriptedLoadableModule.__init__(self, parent)
        self.parent.title = "Finite Element Simulator"
        self.parent.categories = ["Simulation"]
        self.parent.dependencies = []
        self.parent.contributors = ["Luc Lafitte (INRIA)"]
        self.parent.helpText = """
This is an example of scripted loadable module bundled in an extension.
See more information in <a href="https://github.com/organization/projectname#FiniteElementSimulator">module documentation</a>.
"""
        self.parent.acknowledgementText = """
This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc., Andras Lasso, PerkLab,
and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
"""


class NeedleItemWidget(qt.QWidget):
    def __init__(self, index, needle, onUpdateCb):
        super().__init__()

        self._index = index
        self._name = f"Needle_{index+1}"
        self._needle = needle
        self._onUpdateCb = onUpdateCb

        self._needle.SetName(self._name)
        self._needle.SetNthControlPointLabel(0, f"{self._name}_tip")
        self._needle.SetNthControlPointLabel(1, f"{self._name}_tail")

        displayNode = needle.GetDisplayNode()
        displayNode.SetPropertiesLabelVisibility(False)
        displayNode.SetPointLabelsVisibility(True)
        displayNode.GetTextProperty().SetShadow(True)

        layout = qt.QHBoxLayout()
        layout.setSpacing(8)
        self.setLayout(layout)

        label = qt.QLabel(self._name)
        layout.addWidget(label)

        permuteBtn = self._createButton("PermuteTipTail.png", self._onPermute)
        permuteBtn.setToolTip(f"permute tip/tail")
        layout.addWidget(permuteBtn)

        removeBtn = self._createButton("Trash.png", self._onRemove)
        removeBtn.setToolTip(f"remove needle")
        layout.addWidget(removeBtn)

    def _createButton(self, icon: str, cb):
        btn = qt.QToolButton()
        btn.setFixedSize(16, 16)
        btn.setIcon(
            qt.QIcon(
                os.path.join(os.path.dirname(__file__),
                             "Resources", "Icons", icon)
            )
        )
        btn.connect("clicked(bool)", cb)
        return btn

    def _onPermute(self):
        tip = [0.0, 0.0, 0.0]
        tail = [0.0, 0.0, 0.0]
        self._needle.GetNthControlPointPositionWorld(0, tip)
        self._needle.GetNthControlPointPositionWorld(1, tail)
        self._needle.SetNthControlPointPositionWorld(
            0, tail[0], tail[1], tail[2])
        self._needle.SetNthControlPointPositionWorld(1, tip[0], tip[1], tip[2])

    def _onRemove(self):
        res = slicer.util.confirmOkCancelDisplay(
            f"Are you sure to remove needle '{self._name}' ?"
        )
        if res is True:
            slicer.mrmlScene.RemoveNode(self._needle)
            self._onUpdateCb()

#
# FiniteElementSimulatorWidget
#


class FiniteElementSimulatorWidget(ScriptedLoadableModuleWidget, VTKObservationMixin):
    """Uses ScriptedLoadableModuleWidget base class, available at:
    https://github.com/Slicer/Slicer/blob/main/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent=None):
        """
        Called when the user opens the module the first time and the widget is initialized.
        """
        ScriptedLoadableModuleWidget.__init__(self, parent)
        # needed for parameter node observation
        VTKObservationMixin.__init__(self)
        self.logic = None
        self._parameterNode = None
        self._updatingGUIFromParameterNode = False

    def setup(self):
        """
        Called when the user opens the module the first time and the widget is initialized.
        """
        ScriptedLoadableModuleWidget.setup(self)

        # Load widget from .ui file (created by Qt Designer).
        # Additional widgets can be instantiated manually and added to self.layout.
        uiWidget = slicer.util.loadUI(
            self.resourcePath('UI/FiniteElementSimulator.ui'))
        self.layout.addWidget(uiWidget)
        self.ui = slicer.util.childWidgetVariables(uiWidget)

        # Set scene in MRML widgets. Make sure that in Qt designer the top-level qMRMLWidget's
        # "mrmlSceneChanged(vtkMRMLScene*)" signal in is connected to each MRML widget's.
        # "setMRMLScene(vtkMRMLScene*)" slot.
        uiWidget.setMRMLScene(slicer.mrmlScene)

        # Create logic class. Logic implements all computations that should be possible to run
        # in batch mode, without a graphical user interface.
        self.logic = FiniteElementSimulatorLogic()

        # Connections

        # These connections ensure that we update parameter node when scene is closed
        self.addObserver(slicer.mrmlScene, slicer.mrmlScene.StartCloseEvent,
                         self.onSceneStartClose)
        self.addObserver(slicer.mrmlScene, slicer.mrmlScene.EndCloseEvent,
                         self.onSceneEndClose)

        # These connections ensure that whenever user changes some settings on the GUI, that is saved in the MRML scene
        # (in the selected parameter node).

        # Buttons
        self.ui.needlesPatateButton.connect('clicked(bool)',
                                            self.onCreatePatateNeedlesButton)
        self.ui.needlesAddButton.connect('clicked(bool)',
                                         self.onAddNeedleButton)
        self.ui.needlesUpdateButton.connect('clicked(bool)',
                                            self.onUpdateNeedlesButton)
        self.ui.needlesClearButton.connect('clicked(bool)',
                                           self.onClearNeedlesButton)
        self.ui.applyButton.connect('clicked(bool)',
                                    self.onApplyButton)

        # Make sure parameter node is initialized (needed for module reload)
        self.initializeParameterNode()

    def cleanup(self):
        """
        Called when the application closes and the module widget is destroyed.
        """
        self.removeObservers()

    def enter(self):
        """
        Called each time the user opens this module.
        """
        # Make sure parameter node exists and observed
        self.initializeParameterNode()

    def exit(self):
        """
        Called each time the user opens a different module.
        """
        # Do not react to parameter node changes (GUI wlil be updated when the user enters into the module)
        self.removeObserver(self._parameterNode, vtk.vtkCommand.ModifiedEvent,
                            self.updateGUIFromParameterNode)

    def onSceneStartClose(self, caller, event):
        """
        Called just before the scene is closed.
        """
        # Parameter node will be reset, do not use it anymore
        self.setParameterNode(None)

    def onSceneEndClose(self, caller, event):
        """
        Called just after the scene is closed.
        """
        # If this module is shown while the scene is closed then recreate a new parameter node immediately
        if self.parent.isEntered:
            self.initializeParameterNode()

    def initializeParameterNode(self):
        """
        Ensure parameter node exists and observed.
        """
        # Parameter node stores all user choices in parameter values, node selections, etc.
        # so that when the scene is saved and reloaded, these settings are restored.

        self.setParameterNode(self.logic.getParameterNode())

        # Select default input nodes if nothing is selected yet to save a few clicks for the user

    def setParameterNode(self, inputParameterNode):
        """
        Set and observe parameter node.
        Observation is needed because when the parameter node is changed then the GUI must be updated immediately.
        """

        if inputParameterNode:
            self.logic.setDefaultParameters(inputParameterNode)

        # Unobserve previously selected parameter node and add an observer to the newly selected.
        # Changes of parameter node are observed so that whenever parameters are changed by a script or any other module
        # those are reflected immediately in the GUI.
        if self._parameterNode is not None:
            self.removeObserver(self._parameterNode, vtk.vtkCommand.ModifiedEvent,
                                self.updateGUIFromParameterNode)
        self._parameterNode = inputParameterNode
        if self._parameterNode is not None:
            self.addObserver(self._parameterNode, vtk.vtkCommand.ModifiedEvent,
                             self.updateGUIFromParameterNode)

        # Initial GUI update
        self.updateGUIFromParameterNode()

    def updateGUIFromParameterNode(self, caller=None, event=None):
        """
        This method is called whenever parameter node is changed.
        The module GUI is updated to show the current state of the parameter node.
        """

        if self._parameterNode is None or self._updatingGUIFromParameterNode:
            return

        # Make sure GUI changes do not call updateParameterNodeFromGUI (it could cause infinite loop)
        self._updatingGUIFromParameterNode = True

        needles = self.getSceneNeedles()
        self.onUpdateNeedlesButton()

        self.ui.needlesClearButton.enabled = len(needles) > 0

        # Update buttons states and tooltips
        if len(needles) > 1:
            self.ui.applyButton.toolTip = "Run simulation"
            self.ui.applyButton.enabled = True
        else:
            self.ui.applyButton.toolTip = "Select at least 2 needles"
            self.ui.applyButton.enabled = False

        # All the GUI updates are done
        self._updatingGUIFromParameterNode = False

    def updateParameterNodeFromGUI(self, caller=None, event=None):
        """
        This method is called when the user makes any change in the GUI.
        The changes are saved into the parameter node (so that they are restored when the scene is saved and loaded).
        """

        if self._parameterNode is None or self._updatingGUIFromParameterNode:
            return

        # Modify all properties in a single batch
        wasModified = self._parameterNode.StartModify()

        self._parameterNode.EndModify(wasModified)

    def onCreatePatateNeedlesButton(self):
        needles_coords = [
            [[-28.14, -60.72, -520.85],  [-24.28, -8.68, -523.57]],
            [[-25.24, -62.33, -533.99], [-23.64, 2.24, -533.54]],
        ]
        for needle_coords in needles_coords:
            needleNode = slicer.mrmlScene.AddNewNodeByClass(
                "vtkMRMLMarkupsLineNode")
            needleNode.CreateDefaultDisplayNodes()
            needleNode.AddControlPointWorld(*needle_coords[0])
            needleNode.AddControlPointWorld(*needle_coords[1])

        self.updateGUIFromParameterNode()

    def onAddNeedleButton(self):
        selectionNode = slicer.mrmlScene.GetNodeByID(
            "vtkMRMLSelectionNodeSingleton")
        selectionNode.SetReferenceActivePlaceNodeClassName(
            "vtkMRMLMarkupsLineNode")
        interactionNode = slicer.mrmlScene.GetNodeByID(
            "vtkMRMLInteractionNodeSingleton"
        )
        interactionNode.SwitchToSinglePlaceMode()

    def getSceneNeedles(self):
        nodes = slicer.mrmlScene.GetNodesByClass("vtkMRMLMarkupsLineNode")
        needles = []
        for i in range(0, nodes.GetNumberOfItems()):
            needles.append(nodes.GetItemAsObject(i))
        return needles

    def getNeedlesCoordinates(self):
        needles = self.getSceneNeedles()

        needles_coords = []
        for needle in needles:
            tip = list(needle.GetNthControlPointPositionWorld(0))
            tail = list(needle.GetNthControlPointPositionWorld(1))
            needles_coords.append([tip, tail])
        return needles_coords

    def onUpdateNeedlesButton(self):
        listWidget = self.ui.needlesListWidget
        needles = self.getSceneNeedles()

        if listWidget.count == len(needles):
            self.updateGUIFromParameterNode()
            return

        if len(needles) == 0:
            listWidget.clear()
            self.updateGUIFromParameterNode()
            return

        for row in range(0, listWidget.count):
            # update existing row
            if row < len(needles):
                item = listWidget.item(row)
                itemWidget = NeedleItemWidget(row, needles[row],
                                              self.onUpdateNeedlesButton)
                listWidget.setItemWidget(item, itemWidget)
            # remove existing row
            else:
                item = listWidget.takeItem(row)
                del item

        # add new item
        if len(needles) > listWidget.count:
            for row in range(listWidget.count, len(needles)):
                itemWidget = NeedleItemWidget(row, needles[row],
                                              self.onUpdateNeedlesButton)
                item = qt.QListWidgetItem()
                item.setSizeHint(itemWidget.sizeHint)
                listWidget.addItem(item)
                listWidget.setItemWidget(item, itemWidget)

        self.updateGUIFromParameterNode()

    def onClearNeedlesButton(self):
        needles = self.getSceneNeedles()
        res = slicer.util.confirmOkCancelDisplay(
            f"Are you sure to remove all needles ({len(needles)}) ?"
        )
        if res is True:
            for needle in needles:
                slicer.mrmlScene.RemoveNode(needle)
            self.onUpdateNeedlesButton()
            self.updateGUIFromParameterNode()

    def onApplyButton(self):
        """
        Run processing when user clicks "Apply" button.
        """
        with slicer.util.tryWithErrorDisplay("Failed to compute results.", waitCursor=True):

            needles_coords = self.getNeedlesCoordinates()

            self.logic.process(needles_coords)


#
# FiniteElementSimulatorLogic
#

class FiniteElementSimulatorLogic(ScriptedLoadableModuleLogic):
    """This class should implement all the actual
    computation done by your module.  The interface
    should be such that other python code can import
    this class and make use of the functionality without
    requiring an instance of the Widget.
    Uses ScriptedLoadableModuleLogic base class, available at:
    https://github.com/Slicer/Slicer/blob/main/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self):
        """
        Called when the logic class is instantiated. Can be used for initializing member variables.
        """
        ScriptedLoadableModuleLogic.__init__(self)

    def setDefaultParameters(self, parameterNode):
        """
        Initialize parameter node with default settings.
        """
        pass

    def process(self,
                needlesCoords: list,
                needlesDiameter=1.06,
                needlesExposureLength=40.0):
        """
        Run the processing algorithm.
        Can be used without GUI widget.
        """

        if len(needlesCoords) < 2:
            raise ValueError(
                f"You must provide at least 2 needles, {len(needlesCoords)} given.")

        import time
        startTime = time.time()
        logging.info('Processing started')

        # Hackatech
        hackatechMesage = "Hackatech : Mettre en place la procédure à partir de ce point d'entrée."
        logging.info(hackatechMesage)
        print(hackatechMesage)

        stopTime = time.time()
        logging.info(
            f'Processing completed in {stopTime-startTime:.2f} seconds')


#
# FiniteElementSimulatorTest
#

class FiniteElementSimulatorTest(ScriptedLoadableModuleTest):
    """
    This is the test case for your scripted module.
    Uses ScriptedLoadableModuleTest base class, available at:
    https://github.com/Slicer/Slicer/blob/main/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setUp(self):
        """ Do whatever is needed to reset the state - typically a scene clear will be enough.
        """
        slicer.mrmlScene.Clear()

    def runTest(self):
        """Run as few or as many tests as needed here.
        """
        self.setUp()
