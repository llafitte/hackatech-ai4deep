# Hackatech Bordeaux 2023

Le Centre Inria de l'université de Bordeaux et l'université de Bordeaux organisent du 15 au 17 novembre 2023 à la Faïencerie (Bordeaux) l'évènement hackAtech, un marathon d'innovation en sciences du numérique, destiné à impulser de nouveaux projets innovants autour des technologies et expertises d'Inria, de l'université de Bordeaux et de leurs partenaires.

- [Le programme](https://hackatechbordeaux.inria.fr/programme/)
- [Liste des projets](https://hackatechbordeaux.inria.fr/les-projets/)

## Planning / présence

Mercredi : Clair, Baud

Jeudi : Luc, Baud (10h-15h), Clair (matin)

Vendredi : Luc, Clair (matin), Baud (aprem jusqu'à 15h30), Clair (15h30 - fin)

## Ouverture

Mercredi 15 novembre 2023 - 18h30 :

- 17h00	Accueil

- 18h00	Ouverture du sprint hackAtech

- 18h30 - Présentation des projets par les porteurs – Constitution des équipes

## Pitch de présentation

```
Intro : l'électroporation est la technologie du futur: 
- en cardiologie elle est en train de remplacer la RadioFréquence
- en oncologie elle permet de remettre dans une stratégie thérapeutique des patients incurables par les méthodes standards car elle préserve la structure vasculaires des tissus.
Il y a un fort besoin d' outil numérique pour planifier et evaluer precocément une procédure.

Principe: le radiologue interventionnel place les aiguilles (3 à 6) pour électrocuter, le problème est qu'une fois que les aiguilles sont en place il n'a pas une vision précise de la zone traitée (car la distribution du champ électrique est plus complexe que la distribution de la chaleur). L'objectif du Hackatech est de fournir un outil de simulation de la zone directement sur l'image.

Objectif: a) Software de réalité augmentée pour l' ablation tumorale. b) Construire une etude de marché et identifier les cibles.

Outils : On vous a préparer un jeu de données médicales (une pomme de terre), et des fichiers de simulations Freefem, Gmsh pour faire le maillage et calculer les doses. Il faut automatiser tout cela d'ici vendredi midi. 
```

# Le projet AI4DeEp

L’ablation tumorale percutanée par électroporation – application d’un choc électrique sur la zone tumorale – est une thérapie prometteuse pour le traitement des tumeurs profondes : hépatiques et pancréatiques. Le projet consiste à combiner des algorithmes rapides et précis de recalage et calcul de dose pour aider le médecin à visualiser la couverture tumorale pendant l’intervention. En parallèle, nous réfléchirons à l’interface utilisateur du futur outil à mettre dans les mains des équipes médicales (radiologues, chirurgiens…) et plus généralement comment amener cette innovation sur le marché.

## Membres

- De Senneville Baudouin, PhD, Senior Research Scientist, CNRS Bordeaux Sud-Ouest, (baudouin.denis-de-senneville@inria.fr)
- Lafitte Luc, Ingénieur de Recherche, INRIA Bordeaux Sud-Ouest (luc.lafitte@inria.fr)
- Poignard Clair, PhD, Senior Research Scientist, INRIA Bordeaux Sud-Ouest clair.(clair.poignard@inria.fr)


## Sources

Dépôt initialisé depuis :

```sh
git clone https://gitlab.inria.fr/llafitte/hackatech-ai4deep.git
```

## Dépendances

- Installer [Slicer 3D](https://download.slicer.org/)
- Installer [FreeFEM](https://doc.freefem.org/introduction/download.html#download)
- Installer [Gmsh](https://gmsh.info/#Download)
- Installer le module **slicer/AI4DeEP** dans slicer via le menu *Developper Tools > Extension Wiazrd* et le bouton *Select extension*.

## Ressources

Pour tester votre code de simulation, vous trouverez dans le [lien suivant](https://filesender.renater.fr/?s=download&token=9cad34c1-0f63-493a-b0c3-8a2eeb90da15) une donnée de test d'une imagerie CBCT d'une pomme de terre.

Des résultats de simulation déjà effectués par le logiciel existant (IRENA) à retrouver dans le dossier **ressources/irena_results**, qui vous permettront de les comaparer avec vos résultats.

Des fichiers d'exemples pour vous aider à répondre aux différentes problématiques.

## Côté technique 

Automatiser (script ou code) la procédure :
- créer un fichier (.geo, format Gmsh) qui contient la géometrie de la scène - [exemple](ressources/geometry_exemple.geo)
- générer le maillage de la scène à partir de Gmsh
- lancer la simulation numérique (.edp format FreeFEM) - [exemple](ressources/simulation_exemple.edp)
- convertir les résultats de doses de FreeFEM (.vtu, surface) en image (matrice) [exemple](ressources/vtu_to_numpy.py)
- comparer les doses avec les références (cf [irena_results](ressources/irena_results/))

## Côté commercial

Une solution logicielle, actuellement en cours de développement, a été testée dans en routine clinique lors d'opération du foie et pancras sur des patients. Les praticiens hospitaliers qui aident aux développement et qui utilise l'outil, l'ont acceuillit très favorablement et sont convaincu par la pertinence d'une telle solution.

Dans le cadre d'une commercialisation plusieurs questions se posent alors :
- quel produit : solution logicielle clé en main, au cas par cas (sur mesure) ?
- outils de formation pour démocratiser la pratique de l'électroparation et in fine le nombre d'utilsiateur cible ?
- rester 'hardware indépendant' ou se rapprocher d'un frabriquant spécifique ?
- autre ?
