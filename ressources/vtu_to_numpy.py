import vtk
from vtkmodules.util import numpy_support
import numpy as np
from scipy.interpolate import griddata


def vtu_to_numpy(input_vtu, Nx: int, Ny: int, Nz: int, dataname: str):
    """
    Convert a given mesh to an numpy array.

    ...

    Parameters
    ----------
        input_vtu : vtk.vtkStructuredPoints
            A vtk.vtkStructuredPoints object build with vtk.vtkStructuredPointsReader.
        Nx, Ny, Nz : int
            Output numpy array size
        dataname : str
            Data name (in FreeFEM exemple 'u' is used on save vtk file)
    """

    # Read vertices
    vertices = input_vtu.GetCells().GetData()
    vertices = numpy_support.vtk_to_numpy(vertices)
    nb = vertices[0::5]
    vertices1 = vertices[1::5]
    vertices2 = vertices[2::5]
    vertices3 = vertices[3::5]
    vertices4 = vertices[4::5]

    # Read points
    points = input_vtu.GetPoints().GetData()
    points = numpy_support.vtk_to_numpy(points)

    # Read data
    data = input_vtu.GetCellData().GetScalars(dataname)
    data = np.frombuffer(data, dtype=np.float64)

    # Normalize range
    [min_x, max_x, min_y, max_y, min_z, max_z] = input_vtu.GetBounds()
    grid_x, grid_y, grid_z = np.mgrid[0:Nx, 0:Ny, 0:Nz]

    points[:, 0] = ((Nx+1)*(points[:, 0] - min_x))/(max_x - min_x)
    points[:, 1] = ((Ny+1)*(points[:, 1] - min_y))/(max_y - min_y)
    points[:, 2] = ((Nz+1)*(points[:, 2] - min_z))/(max_z - min_z)

    # Interpolate to uniform grid
    interp_points = np.concatenate(
        (points[vertices1, :], points[vertices2, :], points[vertices3, :], points[vertices4, :]), axis=0)
    interp_data = np.concatenate((data, data, data, data), axis=0)

    uniform_data = griddata(interp_points,
                            interp_data,
                            (grid_x, grid_y, grid_z), method='linear')

    uniform_data[np.isnan(uniform_data)] = 0

    return uniform_data
