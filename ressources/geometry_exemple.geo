SetFactory("OpenCASCADE");

// Exemple de fichier qui définit construit une scène contenant :
// - 3 aiguilles (position/orientation et diamètre)
// - une boite de calcul (coodonénes de la boite)

// Needles Diameter
diam=5;

// Tip point of Needle1
xI1=-29;
yI1=-25;
zI1=15;
// orientation of Needle1
vxI1=48;
vyI1=14;
vzI1=1;

// Tip point of Needle2
xI2=-24;
yI2=35;
zI2=-36;
// orientation of Needle2
vxI2=11;
vyI2=-72;
vzI2=17;

// Tip point of Needle3
xI3=-21;
yI3=-4;
zI3= 21;
//orientation of Needle3
vxI3=1;
vyI3=14;
vzI3=-24;

// Mesh Properties
dx=diam/2.;
Mesh.CharacteristicLengthMax = dx;

// Add Needle1
Cylinder(1) = {xI1, yI1, zI1, vxI1, vyI1, vzI1, dx, 2*Pi};
// Add dNeedle2
Cylinder(2) = {xI2, yI2, zI2, vxI2, vyI2, vzI2, dx, 2*Pi};
// Add Needle3
Cylinder(3) = {xI3, yI3, zI3, vxI3, vyI3, vzI3, dx, 2*Pi};

// Add Computation box
Box(10) = {-50, -50, -50, 100, 100, 100};

// Definition of the Needles in the computation box
BooleanIntersection(21) = {Volume{1}; Delete;} {Volume{10};};
BooleanIntersection(22) = {Volume{2}; Delete;} {Volume{10};};
BooleanIntersection(23) = {Volume{3}; Delete;} {Volume{10};};

// Definition of the outer domain
BooleanDifference(31) = {Volume{10}; Delete;} {Volume{21};};
BooleanDifference(32) = {Volume{31}; Delete;} {Volume{22};};
BooleanDifference(33) = {Volume{32}; Delete;} {Volume{23};};

Physical Volume("Needle1", 100) = {21};
Physical Volume("Needle2", 101) = {22};
Physical Volume("Needle3", 102) = {23};
Physical Volume("Medium", 110) = {33};
