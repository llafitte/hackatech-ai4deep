import os
import vtk
import SimpleITK as sitk
from ressources.vtu_to_numpy import vtu_to_numpy

#  PIPELINE 

# générer msh

# creerMailles(needles_coords2, needles_radius,
#              [100., 100., 100.], f"{os.getcwd()}/result.msh")
# TODO récuperer id des aiguilles et medium pour la commande EDP

# lancer edp commande :
# FreeFem++.exe test.edp -nbNeedle 3 -medium 110 -n1 100 -n2 101 -n3 102 -msh .\result.msh

# transfomer vtu en nii.gz

print("reading file ...")

reader = vtk.vtkXMLUnstructuredGridReader()
reader.SetFileName(f"{os.getcwd()}/result.vtu")
reader.Update() 
input_vtk = reader.GetOutput()

print("compute vtu to numpy ...")

sizes = [100, 100, 100]
arr = vtu_to_numpy(input_vtk, *sizes, 'ef')

print("save as .nii.gz ...")

img = sitk.GetImageFromArray(arr)
sitk.WriteImage(img, "result.nii.gz")
