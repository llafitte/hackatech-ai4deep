import os
import gmsh
from itertools import chain

# import pygmsh
# import math


def creerMailles(needles_coords, needles_radius, box_sizes, fileName):

    gmsh.initialize()
    needles_number = len(needles_coords)

    needles_tip = [0]*needles_number
    needles_dir = [0]*needles_number
    for i in range(needles_number):
        needles_tip[i], needles_dir[i] = needles_coords[i][0], [
            e2 - e1 for e1, e2 in zip(needles_coords[i][0], needles_coords[i][1])]

    # box_sizes
    needles_center = needles_tip[0]
    for idx in range(1, needles_number):
        needles_center[0] = needles_center[0] + needles_tip[idx][0]
        needles_center[1] = needles_center[1] + needles_tip[idx][1]
        needles_center[2] = needles_center[2] + needles_tip[idx][2]

    needles_center[0] = needles_center[0] / needles_number
    needles_center[1] = needles_center[1] / needles_number
    needles_center[2] = needles_center[2] / needles_number

    print("needles_center", needles_center)
    box_corner1 = [
        needles_center[0]-box_sizes[0] / 2.0,
        needles_center[1]-box_sizes[1] / 2.0,
        needles_center[2]-box_sizes[2] / 2.0,
    ]

    print("box_corner", box_corner1, box_sizes)

    boxId = gmsh.model.occ.addBox(*box_corner1, *box_sizes)
    print("boxId", boxId)

    cylindersId = [0]*needles_number
    for i in range(needles_number):
        cylindersId[i] = gmsh.model.occ.addCylinder(
            *needles_tip[i], *needles_dir[i], needles_radius)

    print("cylindersId", cylindersId)

    u_needles = [0]*needles_number
    for i in range(needles_number):
        u_needles[i] = 60+i

    print("u_needles", u_needles)

    inters = []
    for i, cylinderId in enumerate(cylindersId):
        inter_id, _ = gmsh.model.occ.intersect([(3, boxId)], [(3, cylinderId)], tag=u_needles[i],
                                               removeTool=True, removeObject=False)
        gmsh.model.occ.synchronize()
        inters.append(inter_id)

    print("inters", inters)

    intersections = [0]*needles_number
    for i in range(needles_number):
        intersections[i] = inters[i][0]

    print("intersection", intersections)

    mediumId = 77
    cut1, cut2 = gmsh.model.occ.cut([(3, boxId)],
                                    intersections, tag=mediumId,
                                    removeObject=True, removeTool=False)
    print("cut", cut1, cut2)
    gmsh.model.occ.synchronize()

    for i in range(needles_number):
        gmsh.model.addPhysicalGroup(
            3, [u_needles[i]], name="Needle"+str(i+1), tag=100+i)
    gmsh.model.addPhysicalGroup(3, [mediumId], name="Medium", tag=110)

    # find surfaces tag of the needles
    surface_needles = []
    for i in range(needles_number):
        up, down = gmsh.model.getAdjacencies(3, u_needles[i])
        surface_needles.append(down)

    surface_needles = list(chain.from_iterable(surface_needles))

    # compute distance from needle surfaces
    distance = gmsh.model.mesh.field.add("Distance")
    gmsh.model.mesh.field.setNumbers(distance, "FacesList", surface_needles)

    # part related to adaptative mesh size
    threshold = gmsh.model.mesh.field.add("Threshold")
    gmsh.model.mesh.field.setNumber(threshold, "InField", distance)
    gmsh.model.mesh.field.setNumber(threshold, "LcMin", needles_radius)
    gmsh.model.mesh.field.setNumber(threshold, "LcMax", 10*needles_radius)
    gmsh.model.mesh.field.setNumber(threshold, "DistMin", 0)
    gmsh.model.mesh.field.setNumber(threshold, "DistMax", 100)

    gmsh.model.mesh.field.setAsBackgroundMesh(threshold)

    gmsh.model.occ.synchronize()

    gmsh.model.mesh.generate(3)
    gmsh.option.setNumber("Mesh.MshFileVersion", 2.2)

    gmsh.write(fileName)


# print("=================== needles 3 ===================")

# needles_radius = 2.5

# needles_coords3 = [
#     [[-29, -25, 15], [48+29, 14+25, 1-15]],
#     [[-24, 35, -36], [11+24, -72-35, 17+36]],
#     [[-21, -4, 21], [1+21, 14+4, -24-21]]
# ]

# creerMailles(needles_coords3, needles_radius,
#               [100., 100., 100.], "test3MESH.msh")

# print("=================== needles 2 ===================")

needles_radius = 0.6

needles_coords2 = [
    [[-28.14, -60.72, -520.85],  [-24.28, -8.68, -523.57]],
    [[-25.24, -62.33, -533.99], [-23.64, 2.24, -533.54]],
]

creerMailles(needles_coords2, needles_radius,
             [100., 100., 100.], f"{os.getcwd()}/result.msh")
